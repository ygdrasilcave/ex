class Mover {
  PVector loc;
  PVector ploc;
  PVector t;
  PVector st;
  PVector w;
  float taily = 0;
  float tailt = 0;
  float tailw = 0;
  color c;

  Mover() {
    loc = new PVector();
    ploc = new PVector();
    t = new PVector(0, 0);
    st = new PVector(random(-0.05, 0.05), random(-0.05, 0.05));
    w = new PVector(random(30, width/2), random(30, height/2));
    if (abs(st.x) > abs(st.y)) {
      tailw = abs(st.x)*400;
    } else {
      tailw = abs(st.y)*400;
    }
    c = color(random(360), 100, 100);
  }

  void display() {
    float x = width/2 + cos(t.x)*w.x;
    float y = height/2 + sin(t.y)*w.y;
    loc.x = x;
    loc.y = y;

    pushMatrix();
    translate(x, y);
    PVector speed = PVector.sub(ploc, loc);
    rotate(speed.heading());

    /////////////////////////////////////////////
    stroke(c);
    strokeWeight(3);
    point(0, 0);
    /////////////////////////////////////////////

    t.add(st);
    popMatrix();
    ploc.x = loc.x;
    ploc.y = loc.y;
  }
}

