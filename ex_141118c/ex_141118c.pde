Mover[] m = new Mover[25];
int b = 0;

void setup() {
  size(800, 800);
  colorMode(HSB, 360, 100, 100, 100);
  background(0,0,0);
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover();
  }
  rectMode(CENTER);
}

void draw() {
  background(0);
  /*noStroke();
  fill(0, 20);
  rect(width/2, height/2, width, height);*/
  if (b == 0) {
    beginShape();
    for (int i=0; i<m.length; i++) {
      m[i].display();
      strokeWeight(0.8);
      fill(0,0,100, 80);
      stroke(0, 0, 100);
      vertex(m[i].loc.x, m[i].loc.y);
    }
    endShape();
  } else if(b == 1){
    for (int i=0; i<m.length; i++) {
      m[i].display();
      if (i%3 == 0 && i < m.length-3) {
        beginShape();
        noStroke();
        fill(0,0,100, 80);
        vertex(m[i].loc.x, m[i].loc.y);
        vertex(m[i+1].loc.x, m[i+1].loc.y);
        vertex(m[i+2].loc.x, m[i+2].loc.y);
        endShape(CLOSE);
      }
    }
  }else if(b == 2){
    for (int i=0; i<m.length; i++) {
      m[i].display();
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    b++;
    if(b > 2){
      b = 0;
    }
  }
}

